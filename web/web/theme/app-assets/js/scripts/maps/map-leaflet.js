/*=========================================================================================
  File Name: map-leaflet.js
  Description: Leaflet Maps
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: hhttp://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(function () {

  if ($('#drag-map').length) {
    let defaultLan = 30.2799991;
    let defaultLng = 57.0664573;
    var draggableMap = L.map('drag-map').setView([defaultLan, defaultLng], 12);
    var markerLocation = L.marker([defaultLan, defaultLng], {
      draggable: 'true'
    }).addTo(draggableMap);

    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
      maxZoom: 18
    }).addTo(draggableMap);

    markerLocation.on('dragend', function (e) {
        console.log('marker.getLatLng', markerLocation.getLatLng())
    });

  }

});
