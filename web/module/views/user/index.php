<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-body">

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'first_name',
            'last_name',
            'username',
            'auth_key',
            'password_hash',
            //'password_reset_token',
            //'email:email',
            //'status',
            //'city_id',
            //'province_id',
            //'address:ntext',
            //'mobile2',
            //'mobile',
            //'domain',
            [
                'format' => 'raw',
                'label' => '*',
                'value' => function($model){
                    $html  = Html::a(Html::encode('ویرایش'), ['update', 'id' => $model->id],['class' => 'btn btn-success']) . ' ';
                    $html .= Html::a(Html::encode('جزیات'), ['view', 'id' => $model->id],['class' => 'btn btn-info']) . ' ';
                    $html .= Html::a('حذف', ['delete', 'id' => $model->id], [
    'class' => 'btn btn-danger',
    'data' => [
    'confirm' => 'آیا از حذف این آیتم مطمئن هستید؟',
    ],
    ]) . ' ';
                    return $html;
                }
            ],
          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
