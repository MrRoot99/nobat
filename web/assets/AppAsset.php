<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/theme/app-assets/vendors/css/vendors-rtl.min.css',
        '/theme/app-assets/vendors/css/charts/apexcharts.css',
        '/theme/app-assets/vendors/css/extensions/toastr.min.css',
//        '/theme/app-assets/vendors/css/tables/datatable/datatables.min.css',
        'theme/app-assets/vendors/css/tables/datatable/responsive.bootstrap.min.css',
        '/theme/app-assets/vendors/css/extensions/sweetalert2.min.css',
        '/theme/app-assets/vendors/css/maps/leaflet.min.css',
        '/theme/app-assets/css-rtl/bootstrap.css',
        '/theme/app-assets/css-rtl/bootstrap-extended.css',
        '/theme/app-assets/css-rtl/colors.css',
        '/theme/app-assets/css-rtl/components.css',
        '/theme/app-assets/css-rtl/themes/dark-layout.css',
        '/theme/app-assets/css-rtl/themes/bordered-layout.css',
        '/theme/app-assets/css-rtl/themes/semi-dark-layout.css',
        '/theme/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css',
        '/theme/app-assets/css-rtl/plugins/charts/chart-apex.css',
        '/theme/app-assets/css-rtl/plugins/extensions/ext-component-toastr.css',
        '/theme/app-assets/css-rtl/pages/app-invoice-list.css',
        '/theme/app-assets/css-rtl/plugins/maps/map-leaflet.css',
        '/theme/app-assets/css-rtl/custom-rtl.css',
        '/theme/assets/css/style-rtl.css',
        '/css/persian-datepicker.min.css',
    ];
    public $js = [
        'js/persian-date.min.js',
        'js/persian-datepicker.min.js',
        'js/bootstrap.js',
        '/theme/app-assets/vendors/js/extensions/sweetalert2.all.min.js'
    ];
//    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//    ];
}
