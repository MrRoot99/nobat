<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Jdf;
use yii\grid\GridView;
use app\models\Turn;

/* @var $this yii\web\View */
/* @var $model app\models\patient */
$this->title = $model->first_name;
$this->params['breadcrumbs'][] = ['label' => 'بیماران', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="card card-body">

    <p>
        <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'آیا از حذف این بیمار مطمعن هستید؟',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            [
                'format' => 'raw',
                'attribute' => 'created_at',
                'value' => function ($model) {
                    if ($model->picture) {
                        return Html::a('مشاهده عکس', $model->getUrl(), ['target' => '_blank']);
                    }
                    return 'بدون عکس';
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return Jdf::jdate("Y/m/d H:i", $model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    return Jdf::jdate("Y/m/d H:i", $model->updated_at);
                }
            ]
//            'deleted_at',
        ],
    ]) ?>

</div>

<div class="card card-body">
    <div class="alert alert-primary" role="alert">
        <div class="alert-body">لیست نوبت های این بیمار:</div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $turnDataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'visitDate',
                'value' => function($model){
                    return Jdf::jdate("Y/m/d",$model->visitDate);
                }
            ],
            [
                'attribute' => 'visitType',
                'filter' => Turn::typeList(),
                'value' => function ($model) {
                    return $model->TypeText;
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Turn::statusList(),
                'value' => function ($model) {
                    return $model->StatusText;
                }
            ],
            [
                'format' => 'raw',
                'label' => '*',
                'value' => function ($model) {
                    return Html::a(Html::encode('جزیات'), ['turn/view', 'id' => $model->id], ['class' => 'btn btn-info']) . ' ';
                }
            ],
            //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>