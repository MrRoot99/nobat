<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\patient */

$this->title = 'بیمار جدید';
$this->params['breadcrumbs'][] = ['label' => 'بیماران', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-create">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => ''
    ]) ?>

</div>
