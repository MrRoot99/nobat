<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PatientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'بیماران';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-body">

    <p>
        <?= Html::a('بیمار جدید', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'first_name',
            'last_name',
            'mobile',
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return \app\components\Jdf::jdate("Y/m/d H:i",$model->created_at);
                }
            ],

            [
                'format' => 'raw',
                'label' => '*',
                'value' => function($model){
                    $html  = Html::a(Html::encode('ویرایش'), ['update', 'id' => $model->id],['class' => 'btn btn-success']) . ' ';
                    $html .= Html::a(Html::encode('جزیات'), ['view', 'id' => $model->id],['class' => 'btn btn-info']) . ' ';
                    $html .= Html::a('حذف', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'آیا از حذف این آیتم مطمئن هستید؟',
                            ],
                        ]) . ' ';
                    return $html;
                }
            ],
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>