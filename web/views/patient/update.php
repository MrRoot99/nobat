<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\patient */

$this->title = 'ویرایش بیمار: ' . $model->first_name;
$this->params['breadcrumbs'][] = ['label' => 'بیماران', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ویرایش';
?>
<div class="patient-update">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => '',
    ]) ?>

</div>
