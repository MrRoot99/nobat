<?php

use app\models\Turn;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Turn */
/* @var $form yii\widgets\ActiveForm */

?>

<?php

$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("pk");
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("بیمار: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("بیمار: " + (index + 1))
    });
});


$("#patientForms").submit(function(event) {

    event.preventDefault(); // stopping submitting
    event.stopImmediatePropagation(); // stopping submitting

    var url = $(this).attr("action");
    $.ajax({
        url: url,
        type: "post",
        contentType: false,
        processData: false,
        data: new FormData(this),
    })
    .done(function(response) {
        if (response.data.success == true) {
        
            // hide Modal 
            $("#patientModal").modal("toggle");
            
            // remove fade 
            $("body").removeClass("modal-open");
            $(".modal-backdrop").remove();

            // show alert
            Swal.fire({
              icon: "success",
              title: "تبریک",
              text: "اطلاعات بیمار با موفقیت ثبت شد. نام بیمار: " + response.data.model.first_name,
              confirmButtonText: "بستن"
            })
        }
        else if (response.data.success == false) {
            Swal.fire({
              icon: "error",
              title: "خطا",
              text: Object.values(response.data.errors),
              confirmButtonText: "بستن"
            })
        }
    })
    .fail(function() {
        console.log("error");
    });

});
    

$("#visitDate").pDatepicker({
    format: "DD , MMMM , YYYY",
    autoClose: true,
    altField: "#visitDateTimeStamp",
    initialValueType: "persian",
    minDate: new persianDate().unix()
});

';

$this->registerJs($js);
?>


<!-- Modal -->
<div class="modal fade" id="patientModal"  data-toggle="modal" stabindex="-1" role="dialog" data-dismiss="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <?= $this->render('/patient/_form', [
                    'model' => $patientModel,
                    'action' => ['patient/create']
                ]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
            </div>

        </div>
    </div>
</div>

<div class="turn-form">

    <div class="row">
        <div class="col-md-6" style="margin-bottom: 20px;direction: ltr">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#patientModal">
                تعریف بیمار جدید
            </button>
        </div>
    </div>

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="row">

        <div class="col-md-4">
            <div class="card card-body">
                <?= $form->field($model, 'visitDate')->textInput(['maxlength' => true,'id'=>'visitDate','value'=>($model->isNewRecord ? '' : \app\components\Jdf::jdate("Y-m-d",$model->visitDate))]) ?>
                <?= $form->field($model, 'visitDateTimeStamp')->hiddenInput(['id'=>'visitDateTimeStamp'])->label(false) ?>

                <?= $form->field($model, 'visitType')->dropDownList(Turn::typeList()) ?>

                <?= $form->field($model, 'status')->dropDownList(Turn::statusList()) ?>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card card-body">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
//            'limit' => 4, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $TurnPeoples[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'patient_id'
                    ],
                ]); ?>
                <div class="panel panel-default">
                    <div class="panel-body container-items"><!-- widgetContainer -->
                        <?php foreach ($TurnPeoples as $index => $EachModel): ?>
                            <div class="item panel panel-default"><!-- widgetBody -->
                                <div>
                                    <div class="row">
                                        <div class="col-md-3" style="margin-bottom: 20px">
                                            <span class="panel-title-address">بیمار: <?= ($index + 1) ?></span> &nbsp;
                                            <button type="button" class="pull-right remove-item btn btn-danger btn-xs">
                                                حذف
                                            </button> &nbsp;
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <?= $form->field($EachModel, "[{$index}]patient_id")->widget(Select2::classname(), [
                                                'data' => \app\models\Patient::getAllUser(),
                                                'initValueText' => '1',
                                                'options' => [
                                                    'placeholder' => 'نام/ شماره بیمار',
                                                    'dir' => 'rtl',
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'minimumInputLength' => 2,
                                                    'ajax' => [
                                                        'url' => Url::to(['/patient/get-patient-list']),
                                                        'dataType' => 'json',
                                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                                    ],
                                                ],
                                            ])->label('نام یا شماره تلفن بیمار را وارید نمایید'); ?>
                                        </div>
                                    </div><!-- end:row -->

                                </div>
                                <hr/>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i
                                        class="fa fa-plus"></i> بیمار جدید برای این نوبت
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                <?php DynamicFormWidget::end(); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('ثبت', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
