<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Turn */

$this->title = 'ایجاد نوبت جدید';
$this->params['breadcrumbs'][] = ['label' => 'نوبت ها', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
    'patientModel' => $patientModel,
    'TurnPeoples' => $TurnPeoples
]) ?>

