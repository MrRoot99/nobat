<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Turn */

$this->title = 'ویراش نوبت شماره: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'نوبت دهی', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' =>'نوبت شماره '. $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ویرایش';
?>

<?= $this->render('_form', [
    'model' => $model,
    'TurnPeoples' => $TurnPeoples,
    'patientModel' => $patientModel,
]) ?>

