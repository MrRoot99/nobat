<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Turn;
use app\components\Jdf;

/* @var $this yii\web\View */
/* @var $model app\models\TurnSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="turn-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-md-3">
            <label class="control-label">از تاریخ</label>
            <input type="text" class="form-control fromTime" placeholder="از تاریخ"  <?= (isset($_GET['TurnSearch']['fromTime']) ? 'value="'. Jdf::jdate("Y-m-d", $_GET['TurnSearch']['fromTime'] / 1000) .'"' : '') ?> />
        </div>
        <div class="col-md-3">
            <label class="control-label">تا تاریخ</label>
            <input type="text" class="form-control toTime" placeholder="تا تاریخ" <?= (isset($_GET['TurnSearch']['toTime']) ? 'value="' .  Jdf::jdate("Y-m-d", $_GET['TurnSearch']['toTime'] / 1000) . '"' : '') ?> />
        </div>

    </div>

    <?= Html::hiddenInput('TurnSearch[fromTime]','',['id'=>'fromTime']); ?>
    <?= Html::hiddenInput('TurnSearch[toTime]','',['id'=>'toTime']); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'visitType')->dropDownList(Turn::typeList(),['prompt' => 'لطفا انتخاب کنید']) ?>

    <?= $form->field($model, 'status')->dropDownList(Turn::statusList(),['prompt' => 'لطفا انتخاب کنید']) ?>

    <div class="form-group">
        <?= Html::submitButton('جست وجو', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
