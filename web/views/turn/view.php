<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\components\Jdf;
/* @var $this yii\web\View */
/* @var $model app\models\Turn */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'نوبت دهی', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="card card-body">

    <p>
        <?= Html::a('ویرایش نوبت', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف نوبت', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'آیا از حذف این آیتم مطمئن هستید؟',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'admin_id',
            [
                'attribute' => 'visitDate',
                'value' => function($model){
                    return Jdf::jdate("Y/m/d",$model->visitDate);
                }
            ],
            [
                'attribute' => 'visitType',
                'value' => $model->TypeText
            ],
            [
                'attribute' => 'status',
                'value' => $model->StatusText
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return Jdf::jdate("Y/m/d",$model->created_at);
                }
            ],
        ],
    ]) ?>

</div>

<div class="card card-body">
    <div class="alert alert-primary" role="alert">
        <div class="alert-body">لیست بیماران:</div>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>شماره بیمار</th>
            <th>نام</th>
            <th>نام خانوادگی</th>
            <th>موبایل</th>
            <th>عکس</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($turnPeoples as $turnPeople): if(!$turnPeople->patient)continue; ?>
            <tr>
                <td><?= Html::a($turnPeople->patient->id,['patient/view','id'=>$turnPeople->patient->id]) ?></td>
                <td><?= $turnPeople->patient->first_name?></td>
                <td><?= $turnPeople->patient->last_name?></td>
                <td><?= $turnPeople->patient->mobile?></td>
                <?php if($turnPeople->patient->picture): ?>
                    <td><?= Html::a('مشاهده عکس',$turnPeople->patient->getUrl(),['target'=>'_blank'])?></td>
                <?php else: ?>
                    <td>بدون عکس</td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>
