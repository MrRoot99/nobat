<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Turn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TurnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'نوبت ها';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-body">
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

</div>
<div class="card card-body">




    <p>
        <?= Html::a('ایجاد نوبت جدید', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'visitDate',
                'value' => function($model){
                    return \app\components\Jdf::jdate("Y/m/d",$model->visitDate);
                }
            ],
            [
                'attribute' => 'visitType',
                'filter' => Turn::typeList(),
                'value' => function($model){
                    return $model->TypeText;
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Turn::statusList(),
                'value' => function($model){
                    return $model->StatusText;
                }
            ],
            [
                'format' => 'raw',
                'label' => '*',
                'value' => function($model){
                    $html  = Html::a(Html::encode('ویرایش'), ['update', 'id' => $model->id],['class' => 'btn btn-success']) . ' ';
                    $html .= Html::a(Html::encode('جزیات'), ['view', 'id' => $model->id],['class' => 'btn btn-info']) . ' ';
                    $html .= Html::a('حذف', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                        'confirm' => 'آیا از حذف این آیتم مطمئن هستید؟',
                        ],
                    ]) . ' ';
                    return $html;
                }
            ],
          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

$js = '
$(".fromTime").pDatepicker({
    format: "DD , MMMM , YYYY",
    autoClose: true,
    altField: "#fromTime",
    initialValueType: "persian"
});
$(".toTime").pDatepicker({
    format: "DD , MMMM , YYYY",
    autoClose: true,
    altField: "#toTime",
    initialValueType: "persian"
});
';

$this->registerJs($js);