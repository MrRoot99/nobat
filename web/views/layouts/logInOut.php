<?php
use yii\bootstrap\ActiveForm;

$BaseUrl = Yii::$app->UrlManager->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="loading" lang="en"  data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <title><?= $this->title ?></title>
    <link rel="apple-touch-icon" href="<?= $BaseUrl ?>/theme/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= $BaseUrl ?>/theme/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/vendors/css/vendors-rtl.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/components.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/pages/page-auth.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/app-assets/css-rtl/custom-rtl.css">
    <link rel="stylesheet" type="text/css" href="<?= $BaseUrl ?>/theme/assets/css/style-rtl.css?v=1.1">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
<?php $this->beginBody() ?>
<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="auth-wrapper auth-v1 px-2">
                <div class="auth-inner py-2">
                    <!-- Register v1 -->
                    <div class="card mb-0">
                        <div class="card-body">
                            <a class="brand-logo">
                                <h2 class="brand-text text-primary ml-1"></h2>
                            </a>

                            <?= $content ?>

                        </div>
                    </div>
                    <!-- /Register v1 -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="<?= $BaseUrl ?>/theme/app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?= $BaseUrl ?>/theme/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?= $BaseUrl ?>/theme/app-assets/js/core/app-menu.js"></script>
<script src="<?= $BaseUrl ?>/theme/app-assets/js/core/app.js"></script>
<!-- END: Theme JS-->
<!-- BEGIN: Page JS-->
<script src="<?= $BaseUrl ?>/theme/app-assets/js/scripts/pages/page-auth-register.js"></script>
<!-- END: Page JS-->

<script>
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    })
</script>
<?php $this->endBody() ?>
</body>
<!-- END: Body-->

</html>
<?php $this->endPage() ?>
