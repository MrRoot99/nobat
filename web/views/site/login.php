<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;



$this->title = 'ورود';
$this->params['breadcrumbs'][] = $this->title;
?>


<h4 class="card-title mb-1">ورود</h4>
<?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= $form->field($model, 'username')->label('موبایل')->textInput(['placeholder'=>'نام کاربری خود را وارد نمایید']) ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'password')->label('رمز عبور')->passwordInput(['placeholder'=>'رمز عبور خود را وارد نمایید']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('ورود', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
    </div>

    <div class="divider my-2">
    </div>

<?php ActiveForm::end(); ?>