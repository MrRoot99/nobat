<?php

/* @var $this yii\web\View */
$BaseUrl = Yii::$app->UrlManager->baseUrl;
$this->title = 'پیشخوان';
?>
<!-- Dashboard Analytics Start -->
<section id="dashboard-analytics">
    <div class="row match-height">
        <!-- Greetings Card starts -->
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card card-congratulations">
                <div class="card-body text-center">
                    <img src="<?= $BaseUrl ?>/theme/app-assets/images/elements/decore-left.png" class="congratulations-img-left" alt="card-img-left" />
                    <img src="<?= $BaseUrl ?>/theme/app-assets/images/elements/decore-right.png" class="congratulations-img-right" alt="card-img-right" />
                    <div class="avatar avatar-xl bg-primary shadow">
                        <div class="avatar-content">
                            <i data-feather="award" class="font-large-1"></i>
                        </div>
                    </div>
                    <div class="text-center">
                        <h1 class="mb-1 text-white">مجموع تعداد بیمار ها: <?= Yii::$app->redis->get('patientCount');?></h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- Greetings Card ends -->


    </div>




</section>
<!-- Dashboard Analytics end -->

