<?php
namespace app\components;

use yii\base\Component;

class Jdf extends Component {

    /* F */
    public static function jdate($format, $timestamp = '', $none = '', $time_zone = 'Asia/Tehran', $tr_num = 'en')
    {
        $T_sec = 0; /* <= &#1585;&#1601;&#1593; &#1582;&#1591;&#1575;&#1610; &#1586;&#1605;&#1575;&#1606; &#1587;&#1585;&#1608;&#1585; &#1548; &#1576;&#1575; &#1575;&#1593;&#1583;&#1575;&#1583; '+' &#1608; '-' &#1576;&#1585; &#1581;&#1587;&#1576; &#1579;&#1575;&#1606;&#1610;&#1607; */
        
        if ($time_zone != 'local')
            date_default_timezone_set(($time_zone == '') ? 'Asia/Tehran' : $time_zone);
        $ts = $T_sec + (($timestamp == '' or $timestamp == 'now') ? time() : self::tr_num($timestamp));
        $date = explode('_', date('H_i_j_n_O_P_s_w_Y', $ts));
        list ($j_y, $j_m, $j_d) = self::gregorian_to_jalali($date[8], $date[3], $date[2]);
        $doy = ($j_m < 7) ? (($j_m - 1) * 31) + $j_d - 1 : (($j_m - 7) * 30) + $j_d + 185;
        $kab = ($j_y % 33 % 4 - 1 == (int) ($j_y % 33 * .05)) ? 1 : 0;
        $sl = strlen($format);
        $out = '';
        for ($i = 0; $i < $sl; $i ++) {
            $sub = substr($format, $i, 1);
            if ($sub == '\\') {
                $out .= substr($format, ++ $i, 1);
                continue;
            }
            switch ($sub) {
                
                case 'E':
                case 'R':
                case 'x':
                case 'X':
                    $out .= 'http://jdf.scr.ir';
                    break;
                
                case 'B':
                case 'e':
                case 'g':
                case 'G':
                case 'h':
                case 'I':
                case 'T':
                case 'u':
                case 'Z':
                    $out .= date($sub, $ts);
                    break;
                
                case 'a':
                    $out .= ($date[0] < 12) ? '&#1602;.&#1592;' : '&#1576;.&#1592;';
                    break;
                
                case 'A':
                    $out .= ($date[0] < 12) ? '&#1602;&#1576;&#1604; &#1575;&#1586; &#1592;&#1607;&#1585;' : '&#1576;&#1593;&#1583; &#1575;&#1586; &#1592;&#1607;&#1585;';
                    break;
                
                case 'b':
                    $out .= (int) ($j_m / 3.1) + 1;
                    break;
                
                case 'c':
                    $out .= $j_y . '/' . $j_m . '/' . $j_d . ' &#1548;' . $date[0] . ':' . $date[1] . ':' . $date[6] . ' ' . $date[5];
                    break;
                
                case 'C':
                    $out .= (int) (($j_y + 99) / 100);
                    break;
                
                case 'd':
                    $out .= ($j_d < 10) ? '0' . $j_d : $j_d;
                    break;
                
                case 'D':
                    $out .= self::jdate_words(array(
                        'kh' => $date[7]
                    ), ' ');
                    break;
                
                case 'f':
                    $out .= self::jdate_words(array(
                        'ff' => $j_m
                    ), ' ');
                    break;
                
                case 'F':
                    $out .= self::jdate_words(array(
                        'mm' => $j_m
                    ), ' ');
                    break;
                
                case 'H':
                    $out .= $date[0];
                    break;
                
                case 'i':
                    $out .= $date[1];
                    break;
                
                case 'j':
                    $out .= $j_d;
                    break;
                
                case 'J':
                    $out .= self::jdate_words(array(
                        'rr' => $j_d
                    ), ' ');
                    break;
                
                case 'k':
                    $out .= self::tr_num(100 - (int) ($doy / ($kab + 365) * 1000) / 10, $tr_num);
                    break;
                
                case 'K':
                    $out .= self::tr_num((int) ($doy / ($kab + 365) * 1000) / 10, $tr_num);
                    break;
                
                case 'l':
                    $out .= self::jdate_words(array(
                        'rh' => $date[7]
                    ), ' ');
                    break;
                
                case 'L':
                    $out .= $kab;
                    break;
                
                case 'm':
                    $out .= ($j_m > 9) ? $j_m : '0' . $j_m;
                    break;
                
                case 'M':
                    $out .= self::jdate_words(array(
                        'km' => $j_m
                    ), ' ');
                    break;
                
                case 'n':
                    $out .= $j_m;
                    break;
                
                case 'N':
                    $out .= $date[7] + 1;
                    break;
                
                case 'o':
                    $jdw = ($date[7] == 6) ? 0 : $date[7] + 1;
                    $dny = 364 + $kab - $doy;
                    $out .= ($jdw > ($doy + 3) and $doy < 3) ? $j_y - 1 : (((3 - $dny) > $jdw and $dny < 3) ? $j_y + 1 : $j_y);
                    break;
                
                case 'O':
                    $out .= $date[4];
                    break;
                
                case 'p':
                    $out .= self::jdate_words(array(
                        'mb' => $j_m
                    ), ' ');
                    break;
                
                case 'P':
                    $out .= $date[5];
                    break;
                
                case 'q':
                    $out .= self::jdate_words(array(
                        'sh' => $j_y
                    ), ' ');
                    break;
                
                case 'Q':
                    $out .= $kab + 364 - $doy;
                    break;
                
                case 'r':
                    $key = self::jdate_words(array(
                        'rh' => $date[7],
                        'mm' => $j_m
                    ));
                    $out .= $date[0] . ':' . $date[1] . ':' . $date[6] . ' ' . $date[4] . ' ' . $key['rh'] . '&#1548; ' . $j_d . ' ' . $key['mm'] . ' ' . $j_y;
                    break;
                
                case 's':
                    $out .= $date[6];
                    break;
                
                case 'S':
                    $out .= '&#1575;&#1605;';
                    break;
                
                case 't':
                    $out .= ($j_m != 12) ? (31 - (int) ($j_m / 6.5)) : ($kab + 29);
                    break;
                
                case 'U':
                    $out .= $ts;
                    break;
                
                case 'v':
                    $out .= self::jdate_words(array(
                        'ss' => substr($j_y, 2, 2)
                    ), ' ');
                    break;
                
                case 'V':
                    $out .= self::jdate_words(array(
                        'ss' => $j_y
                    ), ' ');
                    break;
                
                case 'w':
                    $out .= ($date[7] == 6) ? 0 : $date[7] + 1;
                    break;
                
                case 'W':
                    $avs = (($date[7] == 6) ? 0 : $date[7] + 1) - ($doy % 7);
                    if ($avs < 0)
                        $avs += 7;
                    $num = (int) (($doy + $avs) / 7);
                    if ($avs < 4) {
                        $num ++;
                    } elseif ($num < 1) {
                        $num = ($avs == 4 or $avs == (($j_y % 33 % 4 - 2 == (int) ($j_y % 33 * .05)) ? 5 : 4)) ? 53 : 52;
                    }
                    $aks = $avs + $kab;
                    if ($aks == 7)
                        $aks = 0;
                    $out .= (($kab + 363 - $doy) < $aks and $aks < 3) ? '01' : (($num < 10) ? '0' . $num : $num);
                    break;
                
                case 'y':
                    $out .= substr($j_y, 2, 2);
                    break;
                
                case 'Y':
                    $out .= $j_y;
                    break;
                
                case 'z':
                    $out .= $doy;
                    break;
                
                default:
                    $out .= $sub;
            }
        }
        return ($tr_num != 'en') ? self::tr_num($out, 'fa', '.') : $out;
    }
    /* F */
    public static function jstrftime($format, $timestamp = '', $none = '', $time_zone = 'Asia/Tehran', $tr_num = 'fa')
    {
        $T_sec = 0; /* <= &#1585;&#1601;&#1593; &#1582;&#1591;&#1575;&#1610; &#1586;&#1605;&#1575;&#1606; &#1587;&#1585;&#1608;&#1585; &#1548; &#1576;&#1575; &#1575;&#1593;&#1583;&#1575;&#1583; '+' &#1608; '-' &#1576;&#1585; &#1581;&#1587;&#1576; &#1579;&#1575;&#1606;&#1610;&#1607; */
        
        if ($time_zone != 'local')
            date_default_timezone_set(($time_zone == '') ? 'Asia/Tehran' : $time_zone);
        $ts = $T_sec + (($timestamp == '' or $timestamp == 'now') ? time() : self::tr_num($timestamp));
        $date = explode('_', date('h_H_i_j_n_s_w_Y', $ts));
        list ($j_y, $j_m, $j_d) = self::gregorian_to_jalali($date[7], $date[4], $date[3]);
        $doy = ($j_m < 7) ? (($j_m - 1) * 31) + $j_d - 1 : (($j_m - 7) * 30) + $j_d + 185;
        $kab = ($j_y % 33 % 4 - 1 == (int) ($j_y % 33 * .05)) ? 1 : 0;
        $sl = strlen($format);
        $out = '';
        for ($i = 0; $i < $sl; $i ++) {
            $sub = substr($format, $i, 1);
            if ($sub == '%') {
                $sub = substr($format, ++ $i, 1);
            } else {
                $out .= $sub;
                continue;
            }
            switch ($sub) {
                
                /* Day */
                case 'a':
                    $out .= self::jdate_words(array(
                        'kh' => $date[6]
                    ), ' ');
                    break;
                
                case 'A':
                    $out .= self::jdate_words(array(
                        'rh' => $date[6]
                    ), ' ');
                    break;
                
                case 'd':
                    $out .= ($j_d < 10) ? '0' . $j_d : $j_d;
                    break;
                
                case 'e':
                    $out .= ($j_d < 10) ? ' ' . $j_d : $j_d;
                    break;
                
                case 'j':
                    $out .= str_pad($doy + 1, 3, 0, STR_PAD_LEFT);
                    break;
                
                case 'u':
                    $out .= $date[6] + 1;
                    break;
                
                case 'w':
                    $out .= ($date[6] == 6) ? 0 : $date[6] + 1;
                    break;
                
                /* Week */
                case 'U':
                    $avs = (($date[6] < 5) ? $date[6] + 2 : $date[6] - 5) - ($doy % 7);
                    if ($avs < 0)
                        $avs += 7;
                    $num = (int) (($doy + $avs) / 7) + 1;
                    if ($avs > 3 or $avs == 1)
                        $num --;
                    $out .= ($num < 10) ? '0' . $num : $num;
                    break;
                
                case 'V':
                    $avs = (($date[6] == 6) ? 0 : $date[6] + 1) - ($doy % 7);
                    if ($avs < 0)
                        $avs += 7;
                    $num = (int) (($doy + $avs) / 7);
                    if ($avs < 4) {
                        $num ++;
                    } elseif ($num < 1) {
                        $num = ($avs == 4 or $avs == (($j_y % 33 % 4 - 2 == (int) ($j_y % 33 * .05)) ? 5 : 4)) ? 53 : 52;
                    }
                    $aks = $avs + $kab;
                    if ($aks == 7)
                        $aks = 0;
                    $out .= (($kab + 363 - $doy) < $aks and $aks < 3) ? '01' : (($num < 10) ? '0' . $num : $num);
                    break;
                
                case 'W':
                    $avs = (($date[6] == 6) ? 0 : $date[6] + 1) - ($doy % 7);
                    if ($avs < 0)
                        $avs += 7;
                    $num = (int) (($doy + $avs) / 7) + 1;
                    if ($avs > 3)
                        $num --;
                    $out .= ($num < 10) ? '0' . $num : $num;
                    break;
                
                /* Month */
                case 'b':
                case 'h':
                    $out .= self::jdate_words(array(
                        'km' => $j_m
                    ), ' ');
                    break;
                
                case 'B':
                    $out .= self::jdate_words(array(
                        'mm' => $j_m
                    ), ' ');
                    break;
                
                case 'm':
                    $out .= ($j_m > 9) ? $j_m : '0' . $j_m;
                    break;
                
                /* Year */
                case 'C':
                    $out .= substr($j_y, 0, 2);
                    break;
                
                case 'g':
                    $jdw = ($date[6] == 6) ? 0 : $date[6] + 1;
                    $dny = 364 + $kab - $doy;
                    $out .= substr(($jdw > ($doy + 3) and $doy < 3) ? $j_y - 1 : (((3 - $dny) > $jdw and $dny < 3) ? $j_y + 1 : $j_y), 2, 2);
                    break;
                
                case 'G':
                    $jdw = ($date[6] == 6) ? 0 : $date[6] + 1;
                    $dny = 364 + $kab - $doy;
                    $out .= ($jdw > ($doy + 3) and $doy < 3) ? $j_y - 1 : (((3 - $dny) > $jdw and $dny < 3) ? $j_y + 1 : $j_y);
                    break;
                
                case 'y':
                    $out .= substr($j_y, 2, 2);
                    break;
                
                case 'Y':
                    $out .= $j_y;
                    break;
                
                /* Time */
                case 'H':
                    $out .= $date[1];
                    break;
                
                case 'I':
                    $out .= $date[0];
                    break;
                
                case 'l':
                    $out .= ($date[0] > 9) ? $date[0] : ' ' . (int) $date[0];
                    break;
                
                case 'M':
                    $out .= $date[2];
                    break;
                
                case 'p':
                    $out .= ($date[1] < 12) ? '&#1602;&#1576;&#1604; &#1575;&#1586; &#1592;&#1607;&#1585;' : '&#1576;&#1593;&#1583; &#1575;&#1586; &#1592;&#1607;&#1585;';
                    break;
                
                case 'P':
                    $out .= ($date[1] < 12) ? '&#1602;.&#1592;' : '&#1576;.&#1592;';
                    break;
                
                case 'r':
                    $out .= $date[0] . ':' . $date[2] . ':' . $date[5] . ' ' . (($date[1] < 12) ? '&#1602;&#1576;&#1604; &#1575;&#1586; &#1592;&#1607;&#1585;' : '&#1576;&#1593;&#1583; &#1575;&#1586; &#1592;&#1607;&#1585;');
                    break;
                
                case 'R':
                    $out .= $date[1] . ':' . $date[2];
                    break;
                
                case 'S':
                    $out .= $date[5];
                    break;
                
                case 'T':
                    $out .= $date[1] . ':' . $date[2] . ':' . $date[5];
                    break;
                
                case 'X':
                    $out .= $date[0] . ':' . $date[2] . ':' . $date[5];
                    break;
                
                case 'z':
                    $out .= date('O', $ts);
                    break;
                
                case 'Z':
                    $out .= date('T', $ts);
                    break;
                
                /* Time and Date Stamps */
                case 'c':
                    $key = self::jdate_words(array(
                        'rh' => $date[6],
                        'mm' => $j_m
                    ));
                    $out .= $date[1] . ':' . $date[2] . ':' . $date[5] . ' ' . date('P', $ts) . ' ' . $key['rh'] . '&#1548; ' . $j_d . ' ' . $key['mm'] . ' ' . $j_y;
                    break;
                
                case 'D':
                    $out .= substr($j_y, 2, 2) . '/' . (($j_m > 9) ? $j_m : '0' . $j_m) . '/' . (($j_d < 10) ? '0' . $j_d : $j_d);
                    break;
                
                case 'F':
                    $out .= $j_y . '-' . (($j_m > 9) ? $j_m : '0' . $j_m) . '-' . (($j_d < 10) ? '0' . $j_d : $j_d);
                    break;
                
                case 's':
                    $out .= $ts;
                    break;
                
                case 'x':
                    $out .= substr($j_y, 2, 2) . '/' . (($j_m > 9) ? $j_m : '0' . $j_m) . '/' . (($j_d < 10) ? '0' . $j_d : $j_d);
                    break;
                
                /* Miscellaneous */
                case 'n':
                    $out .= "\n";
                    break;
                
                case 't':
                    $out .= "\t";
                    break;
                
                case '%':
                    $out .= '%';
                    break;
                
                default:
                    $out .= $sub;
            }
        }
        return ($tr_num != 'en') ? self::tr_num($out, 'fa', '.') : $out;
    }
    /* F */
    public static function jmktime($h = '', $m = '', $s = '', $jm = '', $jd = '', $jy = '', $is_dst = -1)
    {
        $h = self::tr_num($h);
        $m = self::tr_num($m);
        $s = self::tr_num($s);
        $jm = self::tr_num($jm);
        $jd = self::tr_num($jd);
        $jy = self::tr_num($jy);
        if ($h == '' and $m == '' and $s == '' and $jm == '' and $jd == '' and $jy == '') {
            return mktime();
        } else {
            list ($year, $month, $day) = self::jalali_to_gregorian($jy, $jm, $jd);
            
            return mktime($h, $m, $s, $month, $day, $year);
        }
    }
    /* F */
    public static function jgetdate($timestamp = '', $none = '', $tz = 'Asia/Tehran', $tn = 'en')
    {
        $ts = ($timestamp == '') ? time() : self::tr_num($timestamp);
        $jdate = explode('_', self::jdate('F_G_i_j_l_n_s_w_Y_z', $ts, '', $tz, $tn));
        return array(
            'seconds' => self::tr_num((int) self::tr_num($jdate[6]), $tn),
            'minutes' => self::tr_num((int) self::tr_num($jdate[2]), $tn),
            'hours' => $jdate[1],
            'mday' => $jdate[3],
            'wday' => $jdate[7],
            'mon' => $jdate[5],
            'year' => $jdate[8],
            'yday' => $jdate[9],
            'weekday' => $jdate[4],
            'month' => $jdate[0],
            0 => self::tr_num($ts, $tn)
        );
    }
    /* F */
    public static function jcheckdate($jm, $jd, $jy)
    {
        $jm = self::tr_num($jm);
        $jd = self::tr_num($jd);
        $jy = self::tr_num($jy);
        $l_d = ($jm == 12) ? (($jy % 33 % 4 - 1 == (int) ($jy % 33 * .05)) ? 30 : 29) : 31 - (int) ($jm / 6.5);
        return ($jm > 0 and $jd > 0 and $jy > 0 and $jm < 13 and $jd <= $l_d) ? true : false;
    }
    /* F */
    public static function tr_num($str, $mod = 'en', $mf = '&#1643;')
    {
        $num_a = array(
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '.'
        );
        $key_a = array(
            '&#1776;',
            '&#1777;',
            '&#1778;',
            '&#1779;',
            '&#1780;',
            '&#1781;',
            '&#1782;',
            '&#1783;',
            '&#1784;',
            '&#1785;',
            $mf
        );
        return ($mod == 'fa') ? str_replace($num_a, $key_a, $str) : str_replace($key_a, $num_a, $str);
    }
    /* F */
    public static function jdate_words($array, $mod = '')
    {
        foreach ($array as $type => $num) {
            $num = (int) self::tr_num($num);
            switch ($type) {
                
                case 'ss':
                    $sl = strlen($num);
                    $xy3 = substr($num, 2 - $sl, 1);
                    $h3 = $h34 = $h4 = '';
                    if ($xy3 == 1) {
                        $p34 = '';
                        $k34 = array(
                            '&#1583;&#1607;',
                            '&#1740;&#1575;&#1586;&#1583;&#1607;',
                            '&#1583;&#1608;&#1575;&#1586;&#1583;&#1607;',
                            '&#1587;&#1740;&#1586;&#1583;&#1607;',
                            '&#1670;&#1607;&#1575;&#1585;&#1583;&#1607;',
                            '&#1662;&#1575;&#1606;&#1586;&#1583;&#1607;',
                            '&#1588;&#1575;&#1606;&#1586;&#1583;&#1607;',
                            '&#1607;&#1601;&#1583;&#1607;',
                            '&#1607;&#1580;&#1583;&#1607;',
                            '&#1606;&#1608;&#1586;&#1583;&#1607;'
                        );
                        $h34 = $k34[substr($num, 2 - $sl, 2) - 10];
                    } else {
                        $xy4 = substr($num, 3 - $sl, 1);
                        $p34 = ($xy3 == 0 or $xy4 == 0) ? '' : ' &#1608; ';
                        $k3 = array(
                            '',
                            '',
                            '&#1576;&#1740;&#1587;&#1578;',
                            '&#1587;&#1740;',
                            '&#1670;&#1607;&#1604;',
                            '&#1662;&#1606;&#1580;&#1575;&#1607;',
                            '&#1588;&#1589;&#1578;',
                            '&#1607;&#1601;&#1578;&#1575;&#1583;',
                            '&#1607;&#1588;&#1578;&#1575;&#1583;',
                            '&#1606;&#1608;&#1583;'
                        );
                        $h3 = $k3[$xy3];
                        $k4 = array(
                            '',
                            '&#1740;&#1705;',
                            '&#1583;&#1608;',
                            '&#1587;&#1607;',
                            '&#1670;&#1607;&#1575;&#1585;',
                            '&#1662;&#1606;&#1580;',
                            '&#1588;&#1588;',
                            '&#1607;&#1601;&#1578;',
                            '&#1607;&#1588;&#1578;',
                            '&#1606;&#1607;'
                        );
                        $h4 = $k4[$xy4];
                    }
                    $array[$type] = (($num > 99) ? str_ireplace(array(
                        '12',
                        '13',
                        '14',
                        '19',
                        '20'
                    ), array(
                        '&#1607;&#1586;&#1575;&#1585; &#1608; &#1583;&#1608;&#1740;&#1587;&#1578;',
                        '&#1607;&#1586;&#1575;&#1585; &#1608; &#1587;&#1740;&#1589;&#1583;',
                        '&#1607;&#1586;&#1575;&#1585; &#1608; &#1670;&#1607;&#1575;&#1585;&#1589;&#1583;',
                        '&#1607;&#1586;&#1575;&#1585; &#1608; &#1606;&#1607;&#1589;&#1583;',
                        '&#1583;&#1608;&#1607;&#1586;&#1575;&#1585;'
                    ), substr($num, 0, 2)) . ((substr($num, 2, 2) == '00') ? '' : ' &#1608; ') : '') . $h3 . $p34 . $h34 . $h4;
                    break;
                
                case 'mm':
                    $key = array(
                        '&#1601;&#1585;&#1608;&#1585;&#1583;&#1740;&#1606;',
                        '&#1575;&#1585;&#1583;&#1740;&#1576;&#1607;&#1588;&#1578;',
                        '&#1582;&#1585;&#1583;&#1575;&#1583;',
                        '&#1578;&#1740;&#1585;',
                        '&#1605;&#1585;&#1583;&#1575;&#1583;',
                        '&#1588;&#1607;&#1585;&#1740;&#1608;&#1585;',
                        '&#1605;&#1607;&#1585;',
                        '&#1570;&#1576;&#1575;&#1606;',
                        '&#1570;&#1584;&#1585;',
                        '&#1583;&#1740;',
                        '&#1576;&#1607;&#1605;&#1606;',
                        '&#1575;&#1587;&#1601;&#1606;&#1583;'
                    );
                    $array[$type] = $key[$num - 1];
                    break;
                
                case 'rr':
                    $key = array(
                        '&#1740;&#1705;',
                        '&#1583;&#1608;',
                        '&#1587;&#1607;',
                        '&#1670;&#1607;&#1575;&#1585;',
                        '&#1662;&#1606;&#1580;',
                        '&#1588;&#1588;',
                        '&#1607;&#1601;&#1578;',
                        '&#1607;&#1588;&#1578;',
                        '&#1606;&#1607;',
                        '&#1583;&#1607;',
                        '&#1740;&#1575;&#1586;&#1583;&#1607;',
                        '&#1583;&#1608;&#1575;&#1586;&#1583;&#1607;',
                        '&#1587;&#1740;&#1586;&#1583;&#1607;',
                        '&#1670;&#1607;&#1575;&#1585;&#1583;&#1607;',
                        '&#1662;&#1575;&#1606;&#1586;&#1583;&#1607;',
                        '&#1588;&#1575;&#1606;&#1586;&#1583;&#1607;',
                        '&#1607;&#1601;&#1583;&#1607;',
                        '&#1607;&#1580;&#1583;&#1607;',
                        '&#1606;&#1608;&#1586;&#1583;&#1607;',
                        '&#1576;&#1740;&#1587;&#1578;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1740;&#1705;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1583;&#1608;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1587;&#1607;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1670;&#1607;&#1575;&#1585;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1662;&#1606;&#1580;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1588;&#1588;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1607;&#1601;&#1578;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1607;&#1588;&#1578;',
                        '&#1576;&#1740;&#1587;&#1578; &#1608; &#1606;&#1607;',
                        '&#1587;&#1740;',
                        '&#1587;&#1740; &#1608; &#1740;&#1705;'
                    );
                    $array[$type] = $key[$num - 1];
                    break;
                
                case 'rh':
                    $key = array(
                        '&#1740;&#1705;&#1588;&#1606;&#1576;&#1607;',
                        '&#1583;&#1608;&#1588;&#1606;&#1576;&#1607;',
                        '&#1587;&#1607; &#1588;&#1606;&#1576;&#1607;',
                        '&#1670;&#1607;&#1575;&#1585;&#1588;&#1606;&#1576;&#1607;',
                        '&#1662;&#1606;&#1580;&#1588;&#1606;&#1576;&#1607;',
                        '&#1580;&#1605;&#1593;&#1607;',
                        '&#1588;&#1606;&#1576;&#1607;'
                    );
                    $array[$type] = $key[$num];
                    break;
                
                case 'sh':
                    $key = array(
                        '&#1605;&#1575;&#1585;',
                        '&#1575;&#1587;&#1576;',
                        '&#1711;&#1608;&#1587;&#1601;&#1606;&#1583;',
                        '&#1605;&#1740;&#1605;&#1608;&#1606;',
                        '&#1605;&#1585;&#1594;',
                        '&#1587;&#1711;',
                        '&#1582;&#1608;&#1705;',
                        '&#1605;&#1608;&#1588;',
                        '&#1711;&#1575;&#1608;',
                        '&#1662;&#1604;&#1606;&#1711;',
                        '&#1582;&#1585;&#1711;&#1608;&#1588;',
                        '&#1606;&#1607;&#1606;&#1711;'
                    );
                    $array[$type] = $key[$num % 12];
                    break;
                
                case 'mb':
                    $key = array(
                        '&#1581;&#1605;&#1604;',
                        '&#1579;&#1608;&#1585;',
                        '&#1580;&#1608;&#1586;&#1575;',
                        '&#1587;&#1585;&#1591;&#1575;&#1606;',
                        '&#1575;&#1587;&#1583;',
                        '&#1587;&#1606;&#1576;&#1604;&#1607;',
                        '&#1605;&#1740;&#1586;&#1575;&#1606;',
                        '&#1593;&#1602;&#1585;&#1576;',
                        '&#1602;&#1608;&#1587;',
                        '&#1580;&#1583;&#1740;',
                        '&#1583;&#1604;&#1608;',
                        '&#1581;&#1608;&#1578;'
                    );
                    $array[$type] = $key[$num - 1];
                    break;
                
                case 'ff':
                    $key = array(
                        '&#1576;&#1607;&#1575;&#1585;',
                        '&#1578;&#1575;&#1576;&#1587;&#1578;&#1575;&#1606;',
                        '&#1662;&#1575;&#1740;&#1740;&#1586;',
                        '&#1586;&#1605;&#1587;&#1578;&#1575;&#1606;'
                    );
                    $array[$type] = $key[(int) ($num / 3.1)];
                    break;
                
                case 'km':
                    $key = array(
                        '&#1601;&#1585;',
                        '&#1575;&#1585;',
                        '&#1582;&#1585;',
                        '&#1578;&#1740;&#8205;',
                        '&#1605;&#1585;',
                        '&#1588;&#1607;&#8205;',
                        '&#1605;&#1607;&#8205;',
                        '&#1570;&#1576;&#8205;',
                        '&#1570;&#1584;',
                        '&#1583;&#1740;',
                        '&#1576;&#1607;&#8205;',
                        '&#1575;&#1587;&#8205;'
                    );
                    $array[$type] = $key[$num - 1];
                    break;
                
                case 'kh':
                    $key = array(
                        '&#1740;',
                        '&#1583;',
                        '&#1587;',
                        '&#1670;',
                        '&#1662;',
                        '&#1580;',
                        '&#1588;'
                    );
                    $array[$type] = $key[$num];
                    break;
                
                default:
                    $array[$type] = $num;
            }
        }
        return ($mod == '') ? $array : implode($mod, $array);
    }
    /**
     * Convertor from and to Gregorian and Jalali (Hijri_Shamsi,Solar) Functions
     * Copyright(C)2011, Reza Gholampanahi [ http://jdf.scr.ir/jdf ] version 2.50
     */
    
    /* F */
    public static function gregorian_to_jalali($g_y, $g_m, $g_d, $mod = '')
    {
        $g_y = self::tr_num($g_y);
        $g_m = self::tr_num($g_m);
        $g_d = self::tr_num($g_d); /* <= :&#1575;&#1610;&#1606; &#1587;&#1591;&#1585; &#1548; &#1580;&#1586;&#1569; &#1578;&#1575;&#1576;&#1593; &#1575;&#1589;&#1604;&#1610; &#1606;&#1610;&#1587;&#1578; */
        $d_4 = $g_y % 4;
        $g_a = array(
            0,
            0,
            31,
            59,
            90,
            120,
            151,
            181,
            212,
            243,
            273,
            304,
            334
        );
        $doy_g = $g_a[(int) $g_m] + $g_d;
        if ($d_4 == 0 and $g_m > 2)
            $doy_g ++;
        $d_33 = (int) ((($g_y - 16) % 132) * .0305);
        $a = ($d_33 == 3 or $d_33 < ($d_4 - 1) or $d_4 == 0) ? 286 : 287;
        $b = (($d_33 == 1 or $d_33 == 2) and ($d_33 == $d_4 or $d_4 == 1)) ? 78 : (($d_33 == 3 and $d_4 == 0) ? 80 : 79);
        if ((int) (($g_y - 10) / 63) == 30) {
            $a --;
            $b ++;
        }
        if ($doy_g > $b) {
            $jy = $g_y - 621;
            $doy_j = $doy_g - $b;
        } else {
            $jy = $g_y - 622;
            $doy_j = $doy_g + $a;
        }
        if ($doy_j < 187) {
            $jm = (int) (($doy_j - 1) / 31);
            $jd = $doy_j - (31 * $jm ++);
        } else {
            $jm = (int) (($doy_j - 187) / 30);
            $jd = $doy_j - 186 - ($jm * 30);
            $jm += 7;
        }
        return ($mod == '') ? array(
            $jy,
            $jm,
            $jd
        ) : $jy . $mod . $jm . $mod . $jd;
    }
    /* F */
    public static function jalali_to_gregorian($j_y, $j_m, $j_d, $mod = '')
    {
        $j_y = self::tr_num($j_y);
        $j_m = self::tr_num($j_m);
        $j_d = self::tr_num($j_d); /* <= :&#1575;&#1610;&#1606; &#1587;&#1591;&#1585; &#1548; &#1580;&#1586;&#1569; &#1578;&#1575;&#1576;&#1593; &#1575;&#1589;&#1604;&#1610; &#1606;&#1610;&#1587;&#1578; */
        $d_4 = ($j_y + 1) % 4;
        $doy_j = ($j_m < 7) ? (($j_m - 1) * 31) + $j_d : (($j_m - 7) * 30) + $j_d + 186;
        $d_33 = (int) ((($j_y - 55) % 132) * .0305);
        $a = ($d_33 != 3 and $d_4 <= $d_33) ? 287 : 286;
        $b = (($d_33 == 1 or $d_33 == 2) and ($d_33 == $d_4 or $d_4 == 1)) ? 78 : (($d_33 == 3 and $d_4 == 0) ? 80 : 79);
        if ((int) (($j_y - 19) / 63) == 20) {
            $a --;
            $b ++;
        }
        if ($doy_j <= $a) {
            $gy = $j_y + 621;
            $gd = $doy_j + $b;
        } else {
            $gy = $j_y + 622;
            $gd = $doy_j - $a;
        }
        foreach (array(
            0,
            31,
            ($gy % 4 == 0) ? 29 : 28,
            31,
            30,
            31,
            30,
            31,
            31,
            30,
            31,
            30,
            31
        ) as $gm => $v) {
            if ($gd <= $v)
                break;
            $gd -= $v;
        }
        return ($mod == '') ? array(
            $gy,
            $gm,
            $gd
        ) : $gy . $mod . $gm . $mod . $gd;
    }
}