<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%turn}}`.
 */
class m211113_094824_create_turn_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%turn}}', [
            'id' => $this->primaryKey(),
            'admin_id' => $this->integer()->unsigned(),
            'visitDate' => $this->string()->notNull(),
            'visitType' => $this->integer()->notNull(),
            'status' => $this->integer(1)->notNull()->unsigned(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
            'deleted_at' => $this->integer()->unsigned(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%turn}}');
    }
}
