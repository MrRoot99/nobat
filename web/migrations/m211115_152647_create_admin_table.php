<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%admin}}`.
 */
class m211115_152647_create_admin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admin}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(),
            'access_token' => $this->string(32),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted_at' => $this->integer()->null(),
        ]);

        $this->insert('{{%admin}}',[
            'name'=> 'MohammadJavad Mirzaei',
            'username' => '09380990138',
            'access_token'=> '0ae1aba6e71c802279b1df5df8a6d311',
            'password_hash'=> '$2y$13$dBvOmKuhcrCbdAwv0yBJLOkQXU8aN2nVWzIZUkBldtdLB3uYamHaO', // init
            'created_at'=> time(),
            'updated_at'=> time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admin}}');
    }
}
