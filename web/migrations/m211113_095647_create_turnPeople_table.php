<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%turnPeople}}`.
 */
class m211113_095647_create_turnPeople_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%turnPeople}}', [
            'id' => $this->primaryKey(),
            'turn_id' => $this->integer()->notNull(),
            'patient_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%turnPeople}}');
    }
}
