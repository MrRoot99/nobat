<?php

namespace app\controllers;


use app\components\Model;
use Yii;
use app\models\Turn;
use app\models\TurnSearch;
use app\models\TurnPeople;
use app\models\Patient;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * TurnController implements the CRUD actions for Turn model.
 */
class TurnController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
        {
            return [
                    'access' => [ 'class' => AccessControl::className(),
                    'user' => Yii::$app->admin,
                    'only' => ['index','create','view','delete','update'],
                    'rules' => [
                    [
                        'actions' => ['index','create','view','delete','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Turn models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TurnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Turn model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $turnPeoples = $model->turnPeople;
        return $this->render('view', [
            'model' => $model,
            'turnPeoples' => $turnPeoples,
        ]);
    }

    /**
     * Creates a new Turn model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Turn();
        $TurnPeoples = [new TurnPeople()];
        $model->status = Turn::STATUS_PENDING;


        if ($model->load(Yii::$app->request->post())) {

            $TurnPeoples = Model::createMultiple(TurnPeople::classname());
            Model::loadMultiple($TurnPeoples, Yii::$app->request->post());
            $isValidDynamicForm = Model::validateMultiple($TurnPeoples);


            if ($isValidDynamicForm && $model->validate() && $model->SaveData($TurnPeoples)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }


        return $this->render('create', [
            'model' => $model,
            'patientModel' => new Patient(),
            'TurnPeoples' => (empty($TurnPeoples)) ? [new TurnPeople] : $TurnPeoples
        ]);
    }

    /**
     * Updates an existing Turn model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        // load turnPeople For Dynamic Form
        $TurnPeoples = $model->turnPeople;


        if ($model->load(Yii::$app->request->post())) {


            $TurnPeoples = Model::createMultiple(TurnPeople::classname(), $TurnPeoples);
            Model::loadMultiple($TurnPeoples, Yii::$app->request->post());
            $isValidDynamicForm = Model::validateMultiple($TurnPeoples);

            if ($isValidDynamicForm && $model->validate() && $model->SaveData($TurnPeoples)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'patientModel' => new Patient(),
            'TurnPeoples' => (empty($TurnPeoples)) ? [new TurnPeople()] : $TurnPeoples
        ]);
    }

    /**
     * Deletes an existing Turn model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted_at = time();
        $model->save(false);
        return $this->redirect(['index?sort=-id']);
    }

    /**
     * Finds the Turn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Turn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Turn::findOne(['id' => $id, 'deleted_at' => null])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
