<?php

namespace app\controllers;

use Yii;
use app\models\Turn;
use app\models\Patient;
use app\models\PatientSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

/**
 * PatientController implements the CRUD actions for Patient model.
 */
class PatientController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [ 'class' => AccessControl::className(),
                'user' => Yii::$app->admin,
                'only' => ['index','create','view','delete','update','get-patient-list'],
                'rules' => [
                    [
                        'actions' => ['index','create','view','delete','update','get-patient-list'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Patient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $turnDataProvider = new ActiveDataProvider([
            'query' => Turn::find()->where(['id'=>array_column($model->turnPeople,'turn_id'),'deleted_at'=>null])
        ]);

        return $this->render('view', [
            'model' => $model,
            'turnDataProvider' => $turnDataProvider
        ]);
    }

    /**
     * Creates a new Patient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Patient();



        if ($model->load(Yii::$app->request->post())) {

            // ajax process
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if ($model->save()) {
                    return [
                        'data' => [
                            'success' => true,
                            'model' => $model,
                        ],
                    ];
                }
                return [
                    'data' => [
                        'success' => false,
                        'errors' => $model->errors,
                    ],
                ];
            }
            // end ajax

            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * ajax search to find Patient
     * @param string $q
     */
    public function actionGetPatientList($q){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $Patients = Patient::find()
            ->OrWhere("`mobile` LIKE '%{$q}%'")
            ->OrWhere("`first_name` LIKE '%{$q}%'")
            ->OrWhere("`last_name` LIKE '%{$q}%'")
            ->andWhere(['deleted_at'=>null])
            ->all();
        $out = [];
        foreach ($Patients as $k => $data) {
            $out['results'][$k]['id'] = $data['id'];
            $out['results'][$k]['text'] = $data['first_name'] . ' ' . $data['last_name'] . ' ( '.  $data['mobile'].' )';
        }
        return $out;
    }
    /**
     * Updates an existing Patient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted_at = time();
        $model->save(false);
        Yii::$app->redis->DECR('patientCount');

        return $this->redirect(['index?sort=-id']);
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return pPatient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Patient::findOne(['id'=>$id,'deleted_at'=>null])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
