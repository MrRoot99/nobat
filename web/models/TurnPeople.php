<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "turnPeople".
 *
 * @property int $id
 * @property int $turn_id
 * @property int $patient_id
 */
class TurnPeople extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'turnPeople';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['patient_id'], 'required'],
            [['turn_id', 'patient_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'turn_id' => 'نوبت',
            'patient_id' => 'بیمار',
        ];
    }
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function getTurn()
    {
        return $this->hasOne(Turn::className(), ['id' => 'turn_id']);
    }
}
