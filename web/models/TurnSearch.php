<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Turn;
use app\components\Jdf;

/**
 * TurnSearch represents the model behind the search form of `app\models\Turn`.
 */
class TurnSearch extends Turn
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'admin_id', 'visitType', 'status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['visitDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Turn::find()->where(['deleted_at'=>null]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'admin_id' => $this->admin_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'visitDate', $this->visitDate]);

        if(isset($params['TurnSearch']['fromTime'])){
            $fromTime = $params['TurnSearch']['fromTime'] / 1000;
            $toTime = $params['TurnSearch']['toTime'] / 1000;
            $fromTime = Jdf::jmktime(0, 0, 0, Jdf::jdate("m", $fromTime), Jdf::jdate("d", $fromTime), Jdf::jdate("Y", $fromTime));
            $toTime = Jdf::jmktime(23, 59, 59, Jdf::jdate("m", $toTime), Jdf::jdate("d", $toTime), Jdf::jdate("Y", $toTime));
            $query->andWhere("$fromTime < visitDate AND visitDate <= $toTime");
        }
        return $dataProvider;
    }
}
